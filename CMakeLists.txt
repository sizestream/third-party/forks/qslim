cmake_minimum_required(VERSION 3.12)
project(qslim)

# Export symbols on Windows
if(WIN32)
  set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS TRUE)
endif()

# Config checks
include(CheckTypeSize)
check_type_size(bool BOOL LANGUAGE CXX)

include(CheckCXXSymbolExists)
check_cxx_symbol_exists(rint cmath HAVE_RINT)
check_cxx_symbol_exists(getrusage "sys/resource.h" HAVE_GETRUSAGE)
if (NOT HAVE_GETRUSAGE)
	check_cxx_symbol_exists(times "sys/times.h" HAVE_TIMES)
endif()

check_cxx_symbol_exists(random "" HAVE_RANDOM)

check_cxx_symbol_exists(hash_map "<hash_map>" HAVE_HASH_MAP)
check_cxx_symbol_exists(hash_set "<hash_set>" HAVE_HASH_SET)
check_cxx_symbol_exists(hash_map "<ext/hash_map>" HAVE_EXT_HASH_MAP)
check_cxx_symbol_exists(hash_set "<ext/hash_set>" HAVE_EXT_HASH_SET)

check_cxx_symbol_exists(valarray "<valarray>" HAVE_VALARRAY)

include(CheckIncludeFileCXX)
check_include_file_cxx("<sstream>" HAVE_SSTREAM)
check_include_file_cxx("<strstream>" HAVE_STRSTREAM)

option(BUILD_GUI "Build with GUI?")
if (BUILD_GUI)
  find_package(FLTK REQUIRED)
  set(HAVE_FLTK ${FLTK_FOUND})

  if ("${FLTK_GL_LIBRARY_RELEASE}" OR "${FLTK_GL_LIBRARY_DEBUG}")
    set(HAVE_FLTK_GL TRUE)
  endif()
endif()

option(BUILD_RASTER "Build with raster features?")
if(BUILD_RASTER)
  find_package(TIFF)
  set(HAVE_LIBTIFF "${TIFF_FOUND}")
  set(HAVE_LIBTIFF_LZW "${TIFF_FOUND}")

  find_package(PNG)
  set(HAVE_LIBPNG "${PNG_FOUND}")

  find_package(JPEG)
  set(HAVE_LIBJPEG "${JPEG_FOUND}")
endif()

option(BUILD_GLTOOLS "Build with OpenGL tools?")
if(BUILD_GLTOOLS)
  find_package(OpenGL REQUIRED COMPONENTS OpenGL)
  set(HAVE_OPENGL "${OPENGL_FOUND}")
  set(CMAKE_REQUIRED_LIBRARIES "${OPENGL_LIBRARIES}")
  check_cxx_symbol_exists(glPolygonOffsetEXT "<hash_map>" HAVE_POLYOFFSET_EXT)
  check_cxx_symbol_exists(glPolygonOffset "<hash_map>" HAVE_POLYOFFSET)

  set(CMAKE_REQUIRED_INCLUDES "${OPENGL_INCLUDE_DIR}")
  check_include_file_cxx("GL/glext.h" HAVE_GL_GLEXT_H)
  check_include_file_cxx("GL/glxext.h" HAVE_GL_GLXEXT_H)
  check_include_file_cxx("GL/wglext.h" HAVE_GL_WGLEXT_H)
endif()

set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
# Descend into subdirectories
add_subdirectory(libgfx)
add_subdirectory(mixkit)

configure_file(qslimConfig.cmake.in qslimConfig.cmake @ONLY)
set(ConfigPackageLocation lib/cmake/qslim)
install(
  FILES ${CMAKE_CURRENT_BINARY_DIR}/qslimConfig.cmake
  DESTINATION ${ConfigPackageLocation})
install(
  EXPORT qslim
  NAMESPACE qslim::
  DESTINATION ${ConfigPackageLocation})
